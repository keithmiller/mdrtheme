<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

					<div id="main" class="eightcol first clearfix" role="main">

						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header">
									<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
								</header>
								<div id="albumByLine">
									<?php 
										$artist = rwmb_meta( 'md_artist' );
										$url = rwmb_meta( 'md_artistUrl' );
										$released = rwmb_meta( 'md_released' );
										// $anchor = bloginfo('template_url');
										// $anchorString = (string)$anchor;
										// $location = "library/16-facebook.png";
										if(!empty($artist) ){
											if (empty($url) && empty($released)){
												echo "<span class='albumArtist'><span class='by'>by: </span>".$artist."</span>";
											} else if(!empty($url) && empty($released)){
												echo "<span class='albumArtist'><a href='".$url."'><span class='by'>by: </span>".$artist."</a></span>";
											} else if(!empty($url) && !empty($released)){
												echo "<span class='albumArtist'><a href='".$url."'><span class='by'>by: </span>".$artist."</a></span>";
												echo "<span class='released'>".$released."</span>";
											} else if(empty($url) && !empty($released)){
												echo "<span class='albumArtist'><span class='by'>by: </span>".$artist."</span>";
												echo "<span class='released'>".$released."</span>";
											} else {

											}
										} else {

										}
										
									?>
								</div>
								
									<?php 
										$artist = rwmb_meta( 'md_artist' );
										if(!empty($artist)) : ?>
											<div class="postPageThumb">
												<?php the_post_thumbnail();  ?>
											</div>
									<?php else : endif; ?>
								<div id='artistPageSocial'>

									<?php if(in_category('Artists')) : ?>
										<div class="artistPageThumb">
											<?php the_post_thumbnail('full');  ?>
										</div>
									

									<?php else : endif; ?>

										<?php
										$facebook = rwmb_meta( 'md_facebook' );
										$twitter = rwmb_meta( 'md_twitter' );
										// $anchor = bloginfo('template_url');
										// $anchorString = (string)$anchor;
										// $location = "library/16-facebook.png";
										$fLink = "http://maddragonrecords.com/wp-content/themes/maddragon2014/library/facebook_round.png";
										$tLink = "http://maddragonrecords.com/wp-content/themes/maddragon2014/library/twitter_round.png";
										if (!empty($facebook) && empty($twitter)){
											echo "<span>Follow: </span>";
											echo "<a href='". $facebook ."' target='blank'><img src='". $fLink ."' alt=''></a>";
											echo "<script>getElementById('artistPageSocial').style.height = 20px;</script>";
										} else if(!empty($facebook) && !empty($twitter)){
											echo "<span>Follow: </span>";
											echo "<a href='". $facebook ."' target='blank'><img src='". $fLink ."' alt=''></a>";
											echo "<a href='". $twitter ."' target='blank'><img src='". $tLink ."' alt=''></a>";
											echo "<script>getElementById('artistPageSocial').style.height = 20px;</script>";
										} else {

										}
									?>
								</div>
	
								<section class="entry-content clearfix" itemprop="articleBody">
									<?php the_content(); ?>
								</section>

								<footer class="article-footer">
									<?php the_tags( '<p class="tags"><span class="tags-title">' . __( 'Tags:', 'bonestheme' ) . '</span> ', ', ', '</p>' ); ?>

								</footer>



							</article>
							<?php 
								if(in_category('Artists')) : 
								$artist = $post->post_name;
								$query = new WP_Query( 'tag='. $artist . '&posts_per_page=2' ); 
									if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
										<div class="album">
											<h4>Albums by This Artist</h4>
											<a href="<?php the_permalink() ?>">
												<?php the_post_thumbnail('bones-thumb-150') ?>
											</a>
										</div>
										<?php endwhile; else : ?>
									<?php endif; wp_reset_postdata(); ?>
								<?php endif; ?>
							<?php endwhile; ?>

						<?php else : ?>

							<article id="post-not-found" class="hentry clearfix">
									<header class="article-header">
										<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
									</header>
									<section class="entry-content">
										<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
									</section>
									<footer class="article-footer">
											<p><?php _e( 'This is the error message in the single.php template.', 'bonestheme' ); ?></p>
									</footer>
							</article>

						<?php endif; ?>

					</div>

				</div>

			</div>

<?php get_footer(); ?>
