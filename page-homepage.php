<?php

/*

Template Name: Homepage Template

*/

?>



<?php get_header(); ?>



			<div id="content" class="homepage">
				<div id="inner-content" class="wrap clearfix">
						<div id="main" class=" first clearfix" role="main">


							<div class="sliderBox"><?php echo do_shortcode('[promoslider display_title="default" display_excerpt="excerpt" height="300px"]'); ?></div>
							<div id="homeBlog" class="homeDiv">
							<h1>Blog</h1>
								<?php $my_query = new WP_Query('category_name=Blog&posts_per_page=15');

  							if ($my_query->have_posts()) : while ($my_query->have_posts()) : $my_query->the_post(); ?>



							<div class="blogPost">

							<article id="post-<?php the_ID(); ?>" class="blogArticle" <?php post_class( 'clearfix' ); ?> role="article">
							<!-- <div class="blogPostThumb"> -->
							<?php 
								if(has_post_thumbnail()): ?>
									<div class='blogPostThumb'><a href="<?php the_permalink() ?>">
									<?php the_post_thumbnail('bones-thumb-150'); ?>
									</a></div>
								<?php else : ?>
									<div></div>
								
							<?php endif; ?>
			
        						<header class="article-header">
									<h2 class="page-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
								</header>



								<section class="entry-content clearfix" itemprop="articleBody">
									
									<?php the_excerpt(); ?>
									

								</section>



								<footer class="article-footer">

									<p class="clearfix"><?php the_tags( '<span class="tags">' . __( 'Tags:', 'bonestheme' ) . '</span> ', ', ', '' ); ?></p>



								</footer>
							</article>

							</div>

							<?php endwhile; ?>
								<div class="clearfix"></div>
							</div>
		
							<div id="homeSide" class="homeDiv">
							<a class="twitter-timeline" href="https://twitter.com/search?q=from%3Amaddragonmusic%20OR%20from%3Amaddragonmedia" data-widget-id="567131136120610816" data-chrome="nofooter" height="500px">Recent Tweets</a>
							<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
							</script>
								<div id="homeListen" class="sideCta">
									<h1>Artists</h1>
									<?php $my_query = new WP_Query('category_name=Artists&posts_per_page=3');

  							while ($my_query->have_posts()) : $my_query->the_post(); ?>



							<div class="homeArtist" class="homeDiv">

							<article id="post-<?php the_ID(); ?>" class="homeAListing" role="article">

								<div class="homeThumb">
									<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('bones-thumb-300'); ?></a>
								</div>
								<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>

							</article>

							</div>

							<?php endwhile; else : ?>



									<article id="post-not-found" class="hentry clearfix">


											<section class="entry-content">

												<p><?php _e( 'The blog is empty right now. Check back soon for posts from our team', 'bonestheme' ); ?></p>

										</section>



									</article>
							<?php endif; ?>

							<a href="http://maddragonrecords.com/mdr-artists/" class="allArtists">See All</a>
								</div>
								<div id="mediaLinksHome">
									<div id="photosLink" class="divLink"><a href="http://maddragonrecords.com/media/pictures/">
										<p></p>
										<h3>Pictures</h3>
										<span></span></a>
									</div>
									<div id="videoLink" class="divLink"><a href="http://maddragonrecords.com/media/videos/">
										<p></p>
										<h3>Videos</h3>
										<span></span></a>
									</div>
									<div id="musicLink" class="divLink"><a href="http://maddragonrecords.com/media/music/">
										<p></p>
										<h3>Music</h3>
										<span></span></a>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div id="homeFooter" class="homeDiv">
							<a href="http://drexel.edu/westphal/undergraduate/MIP/"><span></span></a>
						</div>
					
				</div>
			</div>


<?php get_footer(); ?>

