<?php

/*

Template Name: Artists Template

*/

?>



<?php get_header(); ?>



			<div id="content" class="otherpage">



				<div id="inner-content" class="wrap clearfix">



					<div id="main" class="listArtists first clearfix" role="main">

						
						<h1 class="artists">Artists</h1>
						<div id="currentArtists">
							<h3>Current Artists</h3>
							<?php $my_query = new WP_Query('category_name=Current Artists&posts_per_page=15');

  							while ($my_query->have_posts()) : $my_query->the_post(); ?>



							<div class="artistListing">
								<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><span class="artistLink"></span></a>
								<article id="post-<?php the_ID(); ?>" role="article">

	        						

									<div class="thumbnail">
										<?php the_post_thumbnail('bones-thumb-300'); ?>
									</div>
									<span class="listingTitle"><?php the_title(); ?></h5>



								
								</article>
								<div class='artistSocial'>
								<?php 
										$facebook = rwmb_meta( 'md_facebook' );
										$twitter = rwmb_meta( 'md_twitter' );
										// $anchor = bloginfo('template_url');
										// $anchorString = (string)$anchor;
										// $location = "library/16-facebook.png";
										$fLink = "http://maddragonrecords.com/wp-content/themes/maddragon2014/library/facebook_round.png";
										$tLink = "http://maddragonrecords.com/wp-content/themes/maddragon2014/library/twitter_round.png";
										if (!empty($facebook) && empty($twitter)){
											echo "<a href='". $facebook ."' target='blank'><img src='". $fLink ."' alt=''></a>";
										} else if(!empty($facebook) && !empty($twitter)){
											echo "<a href='". $facebook ."' target='blank'><img src='". $fLink ."' alt=''></a>";
											echo "<a href='". $twitter ."' target='blank'><img src='". $tLink ."' alt=''></a>";
										} else {

										}

										 ?>
								</div>
							</div>

							<?php endwhile; ?>

						</div> <!-- end current artists -->
						<div id="pastArtists">
							<h3>Past Artists</h3>
							<?php $my_query = new WP_Query('category_name=Past Artists&posts_per_page=15');

  							while ($my_query->have_posts()) : $my_query->the_post(); ?>



							<div class="artistListing">
								<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><span class="artistLink"></span></a>
								<article id="post-<?php the_ID(); ?>" role="article">

	        						

									<div class="thumbnail">
										<?php the_post_thumbnail('bones-thumb-300'); ?>
									</div>
									<span class="listingTitle"><?php the_title(); ?></h5>



								
								</article>
								<div class='artistSocial'>
								<?php 
										$facebook = rwmb_meta( 'md_facebook' );
										$twitter = rwmb_meta( 'md_twitter' );
										// $anchor = bloginfo('template_url');
										// $anchorString = (string)$anchor;
										// $location = "library/16-facebook.png";
										$fLink = "http://maddragonrecords.com/wp-content/themes/maddragon2014/library/facebook_round.png";
										$tLink = "http://maddragonrecords.com/wp-content/themes/maddragon2014/library/twitter_round.png";
										if (!empty($facebook) && empty($twitter)){
											echo "<a href='". $facebook ."' target='blank'><img src='". $fLink ."' alt=''></a>";
										} else if(!empty($facebook) && !empty($twitter)){
											echo "<a href='". $facebook ."' target='blank'><img src='". $fLink ."' alt=''></a>";
											echo "<a href='". $twitter ."' target='blank'><img src='". $tLink ."' alt=''></a>";
										} else {

										}

										 ?>
								</div>
							</div>

							<?php endwhile; ?>

						</div> <!-- end past artists -->
					</div> <!-- end main -->
				</div> <!-- end inner-content -->
			</div> <!-- end content -->



<?php get_footer(); ?>

