<?php

/*

Template Name: Blog Template

*/

?>



<?php get_header(); ?>



			<div id="content" class="otherpage">



				<div id="inner-content" class="wrap clearfix">



						<div id="main" class="first clearfix" role="main">


						<h1 class="blogTitle">Blog</h1>

						<?php $my_query = new WP_Query('category_name=Blog&posts_per_page=15');

  							 if ($my_query->have_posts()) : while ($my_query->have_posts()) : $my_query->the_post(); ?>



							<div class="blogPost">

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article">

        						<header class="article-header">



									<h2 class="page-title"><?php the_title(); ?></h2>

								



								</header>



								<section class="entry-content clearfix" itemprop="articleBody">

									<?php the_content(); ?>

								</section>



								<footer class="article-footer">

									<p class="clearfix"><?php the_tags( '<span class="tags">' . __( 'Tags:', 'bonestheme' ) . '</span> ', ', ', '' ); ?></p>



								</footer>



						



							</article>

							</div>

							<?php endwhile; else :?>
							<article id="post-not-found" class="hentry clearfix">

											<header class="article-header">

												<h1><?php _e( 'The blog is empty', 'bonestheme' ); ?></h1>

										</header>

											<section class="entry-content">

												<p><?php _e( 'Check back soon for posts from our team', 'bonestheme' ); ?></p>

										</section>


									</article>

							<?php endif; ?>
								<div class="clearfix"></div>

							</div>



				</div>



			</div>



<?php get_footer(); ?>

