<?php

/*

Template Name: Music/Albums Template

*/

?>



<?php get_header(); ?>



			<div id="content" class="otherpage">
				<div id="inner-content" class="wrap clearfix">
					<div id="main" class="first clearfix" role="main">
						<h1 class="music">Music</h1>
						<div id="listAlbums">
						<?php $my_query = new WP_Query('category_name=Albums&posts_per_page=15');

  							while ($my_query->have_posts()) : $my_query->the_post(); ?>
								<div class="albumListing">
									<article id="post-<?php the_ID(); ?>" role="article">
										<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" class="hoverEffect"></a>
										<div class="albumThumbnail">
											<?php the_post_thumbnail('bones-thumb-125'); ?>
										</div>
										<a class= "albumLink" href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
										<br>by <a class="albumArtistLink" href="<?php echo rwmb_meta( 'md_artistUrl' );?>"><?php echo rwmb_meta( 'md_artist' ); ?></a>
									</article>
								</div>
							<?php endwhile; ?>
							
						</div>
					</div>		
				</div>
			</div>



<?php get_footer(); ?>

