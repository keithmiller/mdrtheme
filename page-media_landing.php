<?php
/*
Template Name: Media Landing Template

Description: This template is the page media landing page with direction to the other media 
*/
?>

<?php get_header(); ?>

	<div id="content">
        <div id="inner-content" class="wrap clearfix">
			<div id="main" class=" first clearfix" role="main">
				<h1>Media</h1>
				<div id="mediaLinks">
					<div id="photosLink" class="divLink"><a href="http://maddragonrecords.com/media/pictures/">
						<p></p>
						<h3>Pictures</h3>
						<span></span></a>
					</div>
					<div id="videoLink" class="divLink"><a href="http://maddragonrecords.com/media/videos/">
						<p></p>
						<h3>Videos</h3>
						<span></span></a>
					</div>
					<div id="musicLink" class="divLink"><a href="http://maddragonrecords.com/media/music/">
						<p></p>
						<h3>Music</h3>
						<span></span></a>
					</div>
				</div>
			</div>
		</div>
	</div><!-- Page Div -->

<?php get_footer(); ?>
