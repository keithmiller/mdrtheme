<?php
/**
* Registering meta boxes
*
* All the definitions of meta boxes are listed below with comments.
* Please read them CAREFULLY.
*
* You also should read the changelog to know what has been changed before updating.
*
* For more information, please visit:
* @link http://www.deluxeblogtips.com/meta-box/
*/


add_filter( 'rwmb_meta_boxes', 'md_register_meta_boxes' );

/**
* Register meta boxes
*
* @return void
*/
function md_register_meta_boxes( $meta_boxes )
{
/**
* Prefix of meta keys (optional)
* Use underscore (_) at the beginning to make keys hidden
* Alt.: You also can make prefix empty to disable it
*/
// Better has an underscore as last sign
$prefix = 'md_';

// 1st meta box
$meta_boxes[] = array(
// Meta box id, UNIQUE per meta box. Optional since 4.1.5
'id' => 'standard',

// Meta box title - Will appear at the drag and drop handle bar. Required.
'title' => __( 'Social Media Links', 'rwmb' ),

// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
'pages' => array( 'post', 'page' ),

// Where the meta box appear: normal (default), advanced, side. Optional.
'context' => 'normal',

// Order of meta box: high (default), low. Optional.
'priority' => 'high',

// Auto save: true, false (default). Optional.
'autosave' => true,

// List of meta fields
'fields' => array(
// TEXT
array(
// Field name - Will be used as label
'name' => __( 'Facebook Link', 'rwmb' ),
// Field ID, i.e. the meta key
'id' => "{$prefix}facebook",
'type' => 'text',
// Default value (optional)
'std' => __( '', 'rwmb' ),
// CLONES: Add to make the field cloneable (i.e. have multiple value)
'clone' => false,
),
// TEXT
array(
// Field name - Will be used as label
'name' => __( 'Twitter Link', 'rwmb' ),
// Field ID, i.e. the meta key
'id' => "{$prefix}twitter",
'type' => 'text',
// Default value (optional)
'std' => __( '', 'rwmb' ),
// CLONES: Add to make the field cloneable (i.e. have multiple value)
'clone' => false,
)
)
);

// 2nd meta box
$meta_boxes[] = array(


// Meta box title - Will appear at the drag and drop handle bar. Required.
'title' => __( 'Album Info', 'rwmb' ),

// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
'pages' => array( 'post', 'page' ),

// Where the meta box appear: normal (default), advanced, side. Optional.
'context' => 'normal',

// Order of meta box: high (default), low. Optional.
'priority' => 'high',

// Auto save: true, false (default). Optional.
'autosave' => true,

// List of meta fields
'fields' => array(
// TEXT
array(
// Field name - Will be used as label
'name' => __( 'Album Artist', 'rwmb' ),
// Field ID, i.e. the meta key
'id' => "{$prefix}artist",
'type' => 'text',
// Default value (optional)
'std' => __( '', 'rwmb' ),
// CLONES: Add to make the field cloneable (i.e. have multiple value)
'clone' => false,
),
// TEXT
array(
// Field name - Will be used as label
'name' => __( 'Artist Page Url', 'rwmb' ),
// Field ID, i.e. the meta key
'id' => "{$prefix}artistUrl",
'type' => 'text',
// Default value (optional)
'std' => __( '', 'rwmb' ),
// CLONES: Add to make the field cloneable (i.e. have multiple value)
'clone' => false,
),
// TEXT
array(
// Field name - Will be used as label
'name' => __( 'Release Year', 'rwmb' ),
// Field ID, i.e. the meta key
'id' => "{$prefix}released",
'type' => 'text',
// Default value (optional)
'std' => __( '', 'rwmb' ),
// CLONES: Add to make the field cloneable (i.e. have multiple value)
'clone' => false,
)
)
);



return $meta_boxes;
}